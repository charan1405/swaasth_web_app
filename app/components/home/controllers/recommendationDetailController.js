(function() {
    'use strict';

    angular.module('swaasth.home')
        .controller('recommendationDetailController', ['$scope', '$rootScope', 'HomeService', '$stateParams', '$state', function($scope, $rootScope, HomeService, $stateParams, $state ) {
            console.log("recommendationDetailController");

            $scope.selectedTab = "home";
            $scope.switchedTab = 1;
            $scope.id = $stateParams.params;

            var details = HomeService.getDetailNotifications($scope.id)[0];

            $scope.detailNotification = {};

            $scope.goBack = function () {
                $state.go('home.recommendationsList');
            };


            $scope.getDetailNotification = function() {

                console.log('details -->', details);
                if (details) {
                    $scope.detailNotification = details;
                    $('.paragraph')[0].innerHTML = $scope.detailNotification.notification;
                }

            };

	}]);

})();