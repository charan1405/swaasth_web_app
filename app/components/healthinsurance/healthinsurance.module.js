var healthinsuranceModule = angular.module("swaasth.healthinsurance", []);

healthinsuranceModule.config(['$stateProvider', '$urlRouterProvider', 'HealthInsuranceRoutes', 
	function($stateProvider, $urlRouterProvider, HealthInsuranceRoutes) {
	    $stateProvider	    
	        .state(HealthInsuranceRoutes.healthInsurance.name,{

	            url: HealthInsuranceRoutes.healthInsurance.url,
	            templateUrl: 'components/healthinsurance/partials/healthInsurance.html'

	        })
	        
	        .state(HealthInsuranceRoutes.healthInsuranceListView.name, {
	        	url: HealthInsuranceRoutes.healthInsuranceListView.url,
	        	templateUrl: 'components/healthinsurance/partials/healthInsuranceListView.html'
	        })

	        .state(HealthInsuranceRoutes.healthInsuranceDetailView.name, {
	        	url: HealthInsuranceRoutes.healthInsuranceDetailView.url,
	        	templateUrl: 'components/healthinsurance/partials/healthInsuranceDetailView.html'
	        })

	}]);