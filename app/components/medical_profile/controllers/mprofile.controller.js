/**
 * @description
 * Manages the Medical Profile of the user and allows user
 * to updates its Medical details
 *
 * @author
 * Abrajeethan
 */
(function() {
    'use strict';

    angular.module('swaasth.mprofile')
        .controller('MedicalProfileController', ['$scope', '$rootScope', '$filter', '$state', 'MedProfileService', '$stateParams','ProfileService', function($scope, $rootScope, $filter, $state, MedProfileService, $stateParams, ProfileService) {
            console.log("MedicalProfileCtrlCalled");

             $scope.selectedTab = "medical";
             $scope.switchedTab = 2;
            var param = $stateParams.params;
            console.log('MedicalProfileController params', param);

            $scope.getFamilyMember = function () {
                //$rootScope.showLoader();

                localStorage.setItem('mProfileUserId', param);

                //ProfileService.getFamilyMemberList().then(function(res){
/*                ProfileService.getUserDetails(param).then(function(response) {
                    $rootScope.hideLoader();
                    if(response){
                        localStorage.setItem('mProfileUserId', response.id);
                            $scope.currentUser = response;
                    } else {
                        localStorage.setItem('mProfileUserId', $scope.currentUser.id);
                    }
                        
                    }).catch(function(error){
                    $rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });*/

            };
            $scope.getFamilyMember();
            
            $scope.maxDateHeight=new Date().toISOString().substring(0,10);
            $scope.medicalDetailsList = [];

            /* Call Adding New height page */
            $scope.addHeight = function () {
                $state.go('height');
            };

            $scope.mprofile = {
                heightCM:0,
                heightInch:0
            };

            $scope.convertHeights = function(type){

                var retValue = 0;
                if(type=="CM"){
                    var initVal = ($scope.mprofile.heightCM/30.48);
                    var feet = Math.floor(initVal);
                    var inch = Math.round((initVal%1)*12);
                    retValue = Number(feet+"."+inch);
                    $scope.mprofile.heightInch = retValue;
                    console.log("retValue",retValue);
                    $scope.getHeightClass(retValue,type);
                } else {
                    retValue = Math.round(($scope.mprofile.heightInch)*30.48);
                    $scope.mdetails.height.value = $scope.mprofile.heightCM = retValue;
                    $scope.getHeightClass(retValue,type);
                }

            };

            $scope.saveMedProfile= function(data){
                MedProfileService.save(data).then(function(response) {
                    console.info('medprofileController[next] : : ', response);
                    //ProfileService.setProfileProgress($scope.progress.profile);

                    //$state.go($scope.profileScreen[nextScreen]);

                }).catch(function(error) {
                    console.warn('medprofileController[next] : : ', error);
                });
            };

            $scope.showPersonalDetail = function () {
                //familyMemberDetail
                //profile.intro.personal
                //$state.go('profile.intro.personal');
                $state.go('familyMemberDetail', { params: param });
            };

            $scope.showDashboard = function(){
                $state.go('dashboard', { params: param });
            };

        }]);
})();
