/**
 * @description
 * Saves the medical details
 *
 * @author Abrajeethan
 */
(function() {

    'use strict';

    var MedProfileService = function($q, $http, $localStorage, BASE_URL, API) {

        var medProfile = {};
        var listOfmedicalDetails = [];

        var headerObj = {
            'height':"Height",
            'weight':"Weight",
            'heartRate':"Heart Rate",
            'bodyTemparature':"Body Temparature",
            'bp':"Blood Pressure",
            'diagnostic':"Diagnostic Tests",
            'allergy':"Alleries & Conditions"
        };

        medProfile.save = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.addMedicalDetails, data)
                .then(function(res) {
                    console.info('medProfileService [save] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('medProfileService [save] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('medProfileService [save] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        medProfile.deleteM = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.deleteMedicalDetails, data)
                .then(function(res) {
                    console.info('medProfileService [delete] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('medProfileService [delete] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('medProfileService [delete] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }; 

        medProfile.getheaderText = function(type) {
            return headerObj[type];
        };


        medProfile.setProfileProgress = function(progress) {
            $localStorage.profileProgress = +progress;
        };

        medProfile.getProfileProgress = function() {
            return $localStorage.profileProgress;
        };

        medProfile.getProfileMedicalInfo = function (category) {
            var deffered = $q.defer();
            $http
                .post(BASE_URL.url + API.getMedicalDetails, category)
                .then(function (res) {
                    
                    if(res.data.statusCode === 200){
                        deffered.resolve(res.data);
                        listOfmedicalDetails = res.data;
                    } else if(res.data.statusCode === 201){
                        deffered.resolve(res.data);
                    } else {
                        deffered.reject(res.data);
                    }
                }).catch(function(error) {
                    deffered.reject(error);
                });

            return deffered.promise;
        };

        medProfile.getBMI = function (reqObj) {
            var deffered = $q.defer();
            $http
                .post(BASE_URL.url + API.getBMI, reqObj)
                .then(function (res) {
                    
                    if(res.data.statusCode === 200){
                        deffered.resolve(res.data);
                    } else if(res.data.statusCode === 201){
                        deffered.resolve(res.data);
                    } else if(res.data.statusCode === 202) {
                         deffered.resolve(res.data);
                    } else {
                        deffered.reject(res.data);
                    }
                }).catch(function(error) {
                    deffered.reject(error);
                });

            return deffered.promise;
        };

        medProfile.getStoredProfileDetails = function() {
            return listOfmedicalDetails;
        };

        medProfile.getAllAssesmentDetails = function (reqObj) {
            var deffered = $q.defer();
            $http
                .post(BASE_URL.url + API.getFinalAssesmentData, reqObj)
                .then(function (res) {
                    
                    if(res.data.statusCode === 200){
                        deffered.resolve(res.data);
                    } else if(res.data.statusCode === 201){
                        deffered.resolve(res.data);
                    } else {
                        deffered.reject(res.data);
                    }
                }).catch(function(error) {
                    deffered.reject(error);
                });

            return deffered.promise;
        }


        return medProfile;
    };

    MedProfileService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.mprofile')
        .factory('MedProfileService', MedProfileService);
})();
