/*globals angular */
(function() {
    'use strict';

    /**
     * Contains the Constants, which are available across the app.
     * @author Tushar
     */
    angular.module('swaasth')
        .constant('BASE_URL', {

            //Development
            //'url': 'http://localhost:1337/',
            //'url': 'http://50.112.131.131:1337/'
            /*
                admin: admin
                password: password
            */
            //vinay system
           // 'url': 'http://192.168.1.32:1337/'
            //Testing
            'url' : 'http://52.11.22.255:1337/'

            //Staging
            // 'url' : 'http://52.23.235.92:1337/',

            //ssh -i "amudITDev.pem" ubuntu@50.112.131.131
            //ssh -i "amudit.pem" ubuntu@52.11.22.255


        }).constant('API', {

            'login': 'login',
            'logout': 'logout',
            'forgotPassword': 'forgetPassword',
            'checkPassword' : 'checkPassword',
            'verifyOTP': 'verifyOTP',
            'resetPassword': 'resetPassword',
            'updatePersonalInfo': 'updatePersonalinfo',
            'addMedicalDetails':'addMedicalDetails',
            'deleteMedicalDetails':'deleteMedicalDetails',
            'getMedicalDetails':'getMedicalDetails',
            'getFamilyMember': 'getUserFamily',
            'getNotifications': 'getNotifications',
            'getDiagnosticsMetaData': 'getDiagnosticsMetaData',
            'deleteMember': 'deleteUser',
            'getSymptomsList': 'getSymptomsList',
            'getTriggersList': 'getTriggersList',
            'allergyList': 'allergyList',
            'fetchAllergyDetails': 'fetchAllergyDetails',
            'CRUDAllergyDetails': 'CRUDAllergyDetails',
            'socialLogin' : 'socialLogin',
            'checkAuthtoken': 'checkAuthtoken',
            'getBMI': 'getBMI',
            'signup': 'signup',
            'checkUserExists': 'checkUserExists',
            'getFinalAssesmentData' : 'getFinalAssesmentData',
            'addInsurance':'upsertInsurance',
            'fetchInsurance':'fetchInsurance',
            'deleteInsurance':'deleteInsurance',
            'addDeviceToken':'addDeviceToken',
            'getUserDetail': 'getUserDetails'
        }).constant('json1',
    {
        "db":"settings",
        "collection": "globals" ,
        "documents" :
        [
            {
                "name": "user settings",
                "isFirstLogin" : true,
                "screenNumber" : 0,
                "authToken":""
            }
        ]
    });

})();
