(function() {
    'use strict';

	angular.module('swaasth.profile')
        .controller('EditProfileController', ['$scope', '$rootScope', 'MedProfileService', 'ProfileService', '$stateParams', '$state', function($scope, $rootScope, MedProfileService, ProfileService, $stateParams, $state ) {
            console.log("EditProfileController"); 

            $scope.selectedTab = "personal";
            $scope.switchedTab = 2;     
 
            var param = $stateParams.params;
            console.log('EditProfileController param::', param);
          
            
            $scope.headerText = 'Save';//param == 'edit' ? 'Done' : 'Next';


            $scope.getUserDetail = function () {

                var screenNo = $rootScope.$state.$current.data ? $rootScope.$state.$current.data.screenNumber : -1;

                if (param == 'new' && screenNo == 1) {
                    $scope.currentUser ? $scope.currentUser : $scope.setCurrentUser({});
                    //return true;
                } else if(param == 'new') {
                    $scope.currentUser ? $scope.currentUser : $scope.setCurrentUser({});
                    //return true;
                } else if (param == 'edit') {

                    var currentUser = $scope.currentUser;

                    if (Object.keys(currentUser).length) {
                        for (var key in currentUser) {
                            if (key === 'dateOfBirth' && $scope.currentUser[key]) {
                                $scope.currentUser[key] = new Date(currentUser[key]);
                                if ($scope.currentUser[key] == 'Invalid Date') {
                                    $scope.currentUser[key] = '';
                                }
                            }
                        }
                    } else {
                        $state.go('familyMember');
                    }

                    $scope.checkGender();
                    return true;

                } else if ($scope.currentUser && $scope.currentUser.id == param) {
                    
                    var currentUser = $scope.currentUser;

                    if (currentUser) {
                        for (var key in currentUser) {
                            if (key === 'dateOfBirth' && $scope.currentUser[key]) {
                                $scope.currentUser[key] = new Date(currentUser[key]);
                                if ($scope.currentUser[key] == 'Invalid Date') {
                                    $scope.currentUser[key] = '';
                                }
                            }
                        }
                    }

                    $scope.checkGender();
                    return true;
                }  else {

                    $rootScope.showLoader();
                    ProfileService.getUserDetails(param).then(function(response) {
                        $rootScope.hideLoader();
                        console.log('edit prifle controller : : ', response);
                        $scope.editUserDetail = response;
                        $scope.currentUser = response;
                        $scope.setCurrentUser(response);
                        var currentUser = response;

                        console.log('editUserDetail : : ', $scope.editUserDetail);

                        if (currentUser) {
                            for (var key in $scope.editUserDetail) {
                                if (key === 'dateOfBirth' && $scope.editUserDetail[key]) {
                                    $scope.editUserDetail[key] = new Date(currentUser[key]);
                                    if ($scope.editUserDetail[key] == 'Invalid Date') {
                                        $scope.editUserDetail[key] = '';
                                        $scope.currentUser[key] = '';
                                    }
                                }
                            }
                        }
                        $scope.checkGender();
                    }).catch(function(error) {
                        $rootScope.hideLoader();
                    });
                }

                console.log('$scope.currentUser-->', $scope.currentUser);
            };

            //going to next screen
            $scope.next = function () {
                //this function is present in the profile.controller

                if ($state.current.name == "emergencyContact") {
                    $scope.nextSave(param);
                } else {
                    if ($state.current.name == "address") {
                        $scope.nextSave(param);
                    } else {
                        $scope.nextAction(param);
                    }
                }

            };

           

            $scope.getFamilyMember = function () {

                ProfileService.getFamilyMemberList().then(function(res){
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                    $scope.personalInfoDetailsList = res.result;
                    console.log('get profile medical info-->', $scope.personalInfoDetailsList);
                    console.log('get current usere',  $scope.currentUser);
                }).catch(function(error){
                    $rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });

            };

            $scope.getUserDetail();
	}]);

})();