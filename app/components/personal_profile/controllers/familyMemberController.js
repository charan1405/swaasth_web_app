(function() {
    'use strict';

	angular.module('swaasth.profile')
        .controller('familyMemberController', ['$scope', '$rootScope', 'ProfileService', '$stateParams', '$state', function($scope, $rootScope, ProfileService, $stateParams, $state ) {
            console.log("familyMembercontroller");

            $scope.selectedTab = "family";
            $scope.switchedTab = 1;

			$scope.headerText = 'Family Members';
			$rootScope.showLoader();
			$scope.isLoad = true;
            $('#deleteMember').modal('hide');

			$scope.getFamilyMember = function () {

				ProfileService.getFamilyMemberList().then(function(res){
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                    $scope.personalInfoDetailsList = res.result;
                    //$scope.currentUser = {};
                    $scope.setCurrentUser({});
                    $scope.isFemale = false;
                    $scope.isMale = false;
                    $scope.isOthers = false;
                    console.log('get profile medical info-->', $scope.personalInfoDetailsList);
                    console.log('get current usere',  $scope.currentUser);
                }).catch(function(error){
                	$rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });

			};
			$scope.getFamilyMember();

			/*Adding new family memeber*/
			$scope.addMedicalInfo = function () {
                // localStorage.setItem('mProfileUserId', '');
				localStorage.setItem('screenNo', 1);
				$state.go($scope.profileScreen[1],{ params: 'new'});
			};

			$scope.showPersonalDetail = function (list) {
				console.log('list-->', list);
				$state.go('familyMemberDetail', { params: list.user.id });
			}

            $scope.deleteMember = function() {
                console.log('deleteMember');
                $rootScope.showLoader();

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj.userId = $scope.deleteMemberId;

                console.log(reqObj);
                ProfileService.deleteMember(reqObj).then(function(response) {
                    $scope.getFamilyMember();
                }).catch(function(error) {
                    console.warn('error while deleteMember : : ', error);
                });

            };

            $scope.deleteFM = function(list){
            	console.log(list);
            	$scope.deleteMemberId = list.user.id;
            	$('#deleteMember').modal('show');
            }


	}]);

})();