/* Directive for touch scroll */
angular.module('swaasth.profile').directive('touchScroll', function() {
    return {
        restrict: 'A',
        link: function(scope, elm, attrs) {


            function isTouchDevice() {
                try {
                    document.createEvent("TouchEvent");
                    return true;
                } catch (e) {
                    return false;
                }
            }

            function touchScroll() {
                console.info('Its working');
                if (isTouchDevice()) { //if touch events exist...
                    var scrollStartPos = 0;

                    elm[0].addEventListener("touchstart", function(event) {
                        scrollStartPos = this.scrollTop + event.touches[0].pageY;
                        //event.preventDefault();
                    }, false);

                    elm[0].addEventListener("touchend", function(event) {
                        scrollStartPos = this.scrollTop + event.touches[0] ? event.touches[0].pageY : 0;
                        //event.preventDefault();
                    }, false);

                    elm[0].addEventListener("touchmove", function(event) {
                        this.scrollTop = scrollStartPos - event.touches[0].pageY;
                        //event.preventDefault();
                    }, false);
                }
            }

            //On page load
            touchScroll();


        }
    };
}).directive('tgDatepicker', function($filter) {
    return {
        scope: {
            max: '@maxDate',
            min: '@minDate'
        },
        require: 'ngModel',
        link: function(scope, ele, attr, controller) {
            console.info('Datepicker initialized');
            ele.bind('click', function() {
                var maxDate, minDate;
                var options = {};
                maxDate = new Date(scope.max);
                minDate = new Date(scope.min);
                var date = ele[0].value? new Date(ele[0].value) : new Date();

                if (isDesktop) {
                    options = { date: date, mode: 'date' }
                } else {
                    options = { date: date, mode: 'date', androidTheme: window.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT }
                }

                if (minDate) {
                    options['minDate'] = Date.parse(minDate);
                }

                if (maxDate) {
                    options['maxDate'] = Date.parse(maxDate);
                }

                var currentField = $(ele[0]);


                function onSuccess(returnDate) {
                    var newDate = new Date(returnDate);
                    var suffixes = ["th", "st", "nd", "rd"];
                    var dateString = $filter('date')(newDate, 'd MMMM yyyy');
                    var parts = dateString.split(' ');
                    var day = parts[0];
                    var relevantDigits = (day < 30) ? day % 20 : day % 30;
                    var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
                    parts[0] = day + suffix;
                    dateString = parts.join(' ');

                    controller.$setViewValue(newDate);
                    controller.$render();
                    currentField.blur();
                }

                function onError(error) { // Android only
                    controller.$setViewValue('');
                    controller.$render();
                }
                datePicker.show(options, onSuccess, onError);
            });
        }
    }
});
