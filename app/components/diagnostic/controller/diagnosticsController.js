(function() {
    'use strict';

    angular.module('swaasth.diagnostic')
        .controller('diagnosticsController', ['$scope', '$rootScope', 'DiagnosticService', '$stateParams', '$state', function($scope, $rootScope, DiagnosticService, $stateParams, $state ) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
            $scope.intialMetaData = [];
            $scope.testList = [];
            $scope.filteredTestList = [];
            $scope.unitList = [];
            $scope.methodList = [];
            $scope.diagnosticsHeader;

            $scope.getDiagnosticsMetaData = function() {
                $rootScope.showLoader();
                var diagnaticAuth = {
                    "authToken": localStorage.getItem('ngStorage-authToken')
                };
                DiagnosticService.getDiagnosticsMetaData(diagnaticAuth).then(function(response) {
                    $scope.intialMetaData = response.result.diagnosticsList;
                    $rootScope.hideLoader();
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });
            };

            $scope.filterByDiagnostic = function (obj) {
                if (obj.diagnostics === Number($stateParams.id)) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.getStoredTestList = function() {
                $scope.diagnosticsHeader = $stateParams.name;
                $scope.testList = DiagnosticService.getStoredTestList();
                $scope.filteredTestList = $scope.testList.filter($scope.filterByDiagnostic);
            };

    }]);

})();