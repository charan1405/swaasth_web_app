(function() {
    'use strict';

    angular.module('swaasth.diagnostic')
        .controller('diagnosticsAddController', ['$scope', '$rootScope', 'DiagnosticService', '$stateParams', '$state', function($scope, $rootScope, DiagnosticService, $stateParams, $state ) {

            $scope.diagnosticsHeader = $stateParams.params;
            $scope.isShownNotes = false;
            $scope.units = [];
            $scope.units = DiagnosticService.getStoredUnitList();
            $scope.newUnit = '';
            $scope.maxDate = new Date().toISOString().substring(0,10);
            $scope.minDate = new Date($scope.user.dateOfBirth).toISOString().substring(0,10);

            $scope.testName = $stateParams.testName;
            $scope.testId = $stateParams.testId;
            $scope.unitId = $stateParams.unitId;

            $scope.methods = DiagnosticService.getStoredMethodList();



            $scope.getUnitForId = function(unitId) {
                $scope.units = DiagnosticService.getStoredUnitList();
                for(var i = 0; i <= $scope.units.length; i++) {
                    if (Number(unitId) === $scope.units[i].id) {
                        return $scope.units[i].name;
                    }
                }
            };

            /*$scope.getTestName = function(testId) {
                $scope.tests = DiagnosticService.getStoredTestList();
                for(var i = 0; i <= $scope.tests.length; i++) {
                    if (Number(testId) === $scope.tests[i].id) {
                        return $scope.tests[i].name;
                    }
                }
            };*/

            $scope.getDefaultMethod = function(testId) {
                $scope.methods = DiagnosticService.getStoredMethodList();
                for(var i = 0; i <= $scope.methods.length; i++) {
                    if (Number(testId) === $scope.methods[i].id) {
                        return $scope.methods[i].name;
                    }
                }
            };

            /*$scope.getUnitsIdForTest = function(testId) {
                $scope.tests = DiagnosticService.getStoredTestList();
                for(var i = 0; i <= $scope.tests.length; i++) {
                    if ( testId === $scope.tests[i].id) {
                        return $scope.getUnitForId($scope.tests[i].unitId);
                    }
                }
            };*/

            $scope.generateUnitList = function() {
                
            };
            
            var index = $stateParams.index, mData={};
            if(index){
                var details = DiagnosticService.getStoredDiagnosticDetails();
                mData = details.generalInfo[$scope.diagnosticsHeader][index];
            }

            var formatedMesurementTime = '';
            if (mData.measurementTime) {
                formatedMesurementTime =  new Date(mData.measurementTime);
            } else {
                var year = new Date().getFullYear();
                var month = new Date().getMonth();
                var day = new Date().getDay();
                var hours = new Date().getHours();
                var minutes = new Date().getMinutes();
                formatedMesurementTime = new Date(year, month, day, hours, minutes, '00', '00'); //.format('hh:mm A');
            }
            
            $scope.medical = {
                value2: mData.value ? mData.value.value : '',
                unit2: mData.value ? mData.value.unit : $scope.getUnitForId($scope.unitId),
                test: mData.value ? mData.value.test : $scope.testName,
                method: mData.value ? mData.value.method : $scope.getDefaultMethod($scope.testId),
                measurementDate: mData.measurementDate ? new Date(mData.measurementDate) : new Date(),
                measurementTime: formatedMesurementTime,
                note: mData.comments || ''
            }

            $scope.showNotesPage = function() {
                $scope.isShownNotes = true;
            };

            $scope.saveDiagnosticDetail= function(data){

                if ($scope.isShownNotes) {
                    $scope.isShownNotes = false;
                    return true;
                }

                $rootScope.showLoader();

/*Adding Diagnostic Details*/
                var ParamsValue = {
                    test :$scope.medical.test,
                    method :$scope.medical.method,
                    unit : $scope.medical.unit2,
                    value : $scope.medical.value2,
                };

/*Adding parameters to Diagnostic */
                var requestParam = {}; 
                requestParam[$scope.diagnosticsHeader] = swaasthRequestObj["diagnostic"][$scope.diagnosticsHeader];
                requestParam[$scope.diagnosticsHeader].comments = $scope.medical.note;
                requestParam[$scope.diagnosticsHeader].measurementDate = $scope.medical.measurementDate;
                requestParam[$scope.diagnosticsHeader].measurementTime = $scope.medical.measurementTime;
                requestParam[$scope.diagnosticsHeader].value = ParamsValue;

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj[$scope.diagnosticsHeader] = requestParam[$scope.diagnosticsHeader];
                if(index){
                    reqObj['type'] = "update";
                    reqObj['index'] = index;
                }
                reqObj.userId = localStorage.getItem('mProfileUserId');
                reqObj.testId = $scope.testId;
                DiagnosticService.save(reqObj).then(function(response) {
                    console.info('successfullly saved : : ', response);
                    $rootScope.hideLoader();
                    
                    if(index){
                        $scope.goBack();
                    } else {
                        $state.go('diagnosticDetailView', {  params: $scope.diagnosticsHeader, testName: $scope.testName, testId: $scope.testId, unitId: $scope.unitId, index: -2 });
                    }

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });
            };

            $scope.unitSelected = function($event) {

                var uselectedUnit = $($event.currentTarget).attr('value');

                if(uselectedUnit == "others"){
                    $("#addUnitModal").modal('hide');
                    $("#addNewUnitModal").modal('show');
                } else {
                    $scope.medical.unit2 = uselectedUnit;
                    $("#addUnitModal").modal('hide');
                }
            };

            $scope.addNewUnit = function() {
                $scope.medical.unit2 = $scope.newUnit;
            };

            $scope.showMethodList = function() {
                
            };

    }]);

})();