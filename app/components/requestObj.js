

var swaasthRequestObj = {

	"height" : {

		"default" : {
			"unit2" : 'cm',
			"unit1" : 'ft. & in.',
			"header": "Height"
		},
		"request" : {
			"value": '',
	        "comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
    	}
	},

	"weight" : {

		"default" : {
			"unit2" : 'Kg',
			"header": "Weight"
		},

		"request" : {
			"value": '',
	        "comments": '',
	        "measurementDate": ''
	    }
	},

	"heartbeat" : {

		"default" : {
			"unit2" : 'BPM',
			"header": "Heart Rate"
		},

		"request" : {
			"value": '',
	        "comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
    	}
	},

	"bodyTemperature" : {

		"default" : {
			"unit2" : 'C',
			"unit1" : 'F',
			"header": "Body Temperature"
		},

		"request" : {
			"value": '',
	        "comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
    	}
	},

	"bp" : {

		"default" : {
			"unit1" : 'systolic',
			"unit2" : 'diastolic',
			"header": "Blood Pressure"
		},

		"request" : {

			"value": {
					"diastolic" : '',
					"systolic" : ''
			},
			"comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
		}
	},

	"diagnostic" : {
		"default" : {
			"unit2" : 'C',
			"header": "Diagnostic Tests"
		},

		"sugar" : {
			"value": '',
	        "comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
    	}
	},

	"allergies" : {
		"default" : {
			"header": "Alleries & Conditions"
		},

		"request" : {
			"value": '',
	        "comments": '',
	        "measurementDate": '',
	        "measurementTime" : ''
    	}
	}

}