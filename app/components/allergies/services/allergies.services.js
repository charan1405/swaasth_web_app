/**
 * @description 
 *displayin the Allergies Details
 *              
 * @author Koushik
 */
(function() {

    'use strict';

    var AllergyService = function($q, $http, $localStorage, BASE_URL, API) {

        var Allergies = {};
        var allergiesForUser = [];
        var allergiesList = [];
        var symptomsList = [];
        var tiggersList = [];


        Allergies.fetchAllergyDetails = function(usedDetails) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.fetchAllergyDetails, usedDetails)
                .then(function(res) {
                    console.info('Allegies List [fetchAllergyDetails] :: >>', res);
                    if(res.data.statusCode === 200){
                        deferred.resolve(res.data);
                        allergiesForUser = res.data.result.allergies;
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        Allergies.getAllergiesForUser = function() {
            return allergiesForUser;
        };


        Allergies.getAllergiessList = function(user) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.allergyList, user)
                .then(function(res) {
                    console.info('AllergyService [getAllergiessList] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);
                        allergiesList = res.data.allergies;                   
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        Allergies.getStoredAllergiesList = function(){
            return allergiesList;
        };


        Allergies.getSymptomsList = function(user) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.getSymptomsList, user)
                .then(function(res) {
                    console.info('AllergyService [getSymptomsList] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);
                        symptomsList = res.data.symptoms;                   
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        Allergies.getStoredSymptomsList = function(){
            return symptomsList;
        };



        Allergies.getTriggersList = function(allergyId) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.getTriggersList, allergyId)
                .then(function(res) {
                    console.info('AllergyService [getTriggersList] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);
                        tiggersList = res.data.triggers;                   
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        Allergies.getStoredTiggersList = function(){
            return tiggersList;
        };

        Allergies.CRUDAllergyDetails = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.CRUDAllergyDetails, data)
                .then(function(res) {
                    console.info('CRUDAllergyDetails [save] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('CRUDAllergyDetails [save] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('CRUDAllergyDetails [save] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        


        return Allergies;
    };

    AllergyService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.allergies')
        .factory('AllergyService', AllergyService);
})();
